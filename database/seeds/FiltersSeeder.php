<?php

use Illuminate\Database\Seeder;

class FiltersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
    $filtersArray = array(

      array(
        'id' => '1',
        'naam' => 'Bagagekluizen',
        'imageUrl' => '/filters/bagagekluizen.svg',
        'categorie' => 'Service', 'backgroundColor' => '#4B77BE'
      ),
      array(
        'id' => '2',
        'naam' => 'Bemenste fietsenstalling',
        'imageUrl' => '/filters/fiets.svg',
        'categorie' => 'Service', 'backgroundColor' => '#4B77BE',
      ),
      array(
        'id' => '3',
        'naam' => 'Fiets & Service',
        'imageUrl' => '/filters/fiets.svg',
        'categorie' => 'Service', 'backgroundColor' => '#4B77BE',
      ),
      array(
        'id' => '4',
        'naam' => 'Geldautomaat',
        'imageUrl' => '/filters/geldautomaat.svg',
        'categorie' => 'Service', 'backgroundColor' => '#4B77BE',
      ),
      array(
        'id' => '5',
        'naam' => 'Gleidelijnen',
        'imageUrl' => '/filters/gleidelijnen.svg',
        'categorie' => 'Service', 'backgroundColor' => '#4B77BE',
      ),
      array(
        'id' => '6',
        'naam' => 'GWK Travelex',
        'imageUrl' => '/filters/shops.svg',
        'categorie' => 'Service', 'backgroundColor' => '#4B77BE',
      ),
      array(
        'id' => '7',
        'naam' => 'Kaartautomaat NS',
        'imageUrl' => '/filters/ov_kaart_machine.svg',
        'categorie' => 'OV', 'backgroundColor' => '#FFC312',
      ),
      array(
        'id' => '8',
        'naam' => 'Lift',
        'imageUrl' => '/filters/lift.svg',
        'categorie' => 'Service', 'backgroundColor' => '#4B77BE',
      ),
      array(
        'id' => '9',
        'naam' => 'Lift',
        'imageUrl' => '/filters/lift.svg',
        'categorie' => 'Service', 'backgroundColor' => '#4B77BE',
      ),
      array(
        'id' => '10',
        'naam' => 'Onbewaakte fietsenstalling',
        'imageUrl' => '/filters/fiets.svg',
        'categorie' => 'OV', 'backgroundColor' => '#FFC312',
      ),
      array(
        'id' => '11',
        'naam' => 'OV Servicewinkel',
        'imageUrl' => '/filters/service_balie.svg',
        'categorie' => 'Service', 'backgroundColor' => '#4B77BE',
      ),
      array(
        'id' => '12',
        'naam' => 'OV-chipkaart poorten',
        'imageUrl' => '/filters/check_in.svg',
        'categorie' => 'OV', 'backgroundColor' => '#FFC312',
      ),
      array(
        'id' => '13',
        'naam' => 'OV-fiets',
        'imageUrl' => '/filters/service_balie.svg',
        'categorie' => 'OV', 'backgroundColor' => '#FFC312',
      ),
      array(
        'id' => '14',
        'naam' => 'Pasfotostudio',
        'imageUrl' => '/filters/pasfotostudio.svg',
        'categorie' => 'Service', 'backgroundColor' => '#4B77BE',
      ),
      array(
        'id' => '15',
         'naam'=> 'Reisassistentie NS',
         'imageUrl' => '/filters/service_balie.svg',
         'categorie' => 'Service', 'backgroundColor' => '#4B77BE',
     ),
      array(
        'id' => '16',
        'naam' => 'Sanifair Toilet',
        'imageUrl' => '/filters/wc.svg',
        'categorie' => 'Service', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '17',
        'naam' => 'Service- en alarmzuil',
        'imageUrl' => '/filters/service_balie.svg',
        'categorie' => 'Shops', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '18',
        'naam' => 'AKO',
        'imageUrl' => '/filters/shops.svg',
        'categorie' => 'Shops', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '19',
        'naam' => 'Bloemenzaak Langeveld',
        'imageUrl' => '/filters/shops.svg',
        'categorie' => 'Shops', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '20',
        'naam' => 'Etos',
        'imageUrl' => '/filters/shops.svg',
        'categorie' => 'Shops', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '21',
        'naam' => 'HEMA',
        'imageUrl' => '/filters/shops.svg',
        'categorie' => 'Shops', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '22',
        'naam' => 'Rituals',
        'imageUrl' => '/filters/shops.svg',
        'categorie' => 'Shops', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '23',
        'naam' => 'Sissy-Boy',
        'imageUrl' => '/filters/shops.svg',
        'categorie' => 'Shops', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '24',
        'naam' => 'VILA',
        'imageUrl' => '/filters/shops.svg',
        'categorie' => 'Shops', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '25',
        'naam' => 'AH to go',
        'imageUrl' => '/filters/food.svg',
        'categorie' => 'Food', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '26',
        'naam' => 'Broodzaak',
        'imageUrl' => '/filters/food.svg',
        'categorie' => 'Food', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '27',
        'naam' => 'Burger King',
        'imageUrl' => '/filters/food.svg',
        'categorie' => 'Food', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '28',
        'naam' => 'Julia`s',
        'imageUrl' => '/filters/food.svg',
        'categorie' => 'Food', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '29',
        'naam' => 'Kiosk',
        'imageUrl' => '/filters/food.svg',
        'categoie' => 'Food', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '30',
        'naam' => 'La Place',
        'imageUrl' => '/filters/food.svg',
        'categorie' => 'Food', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '31',
        'naam' => 'Smullers',
        'imageUrl' => '/filters/food.svg',
        'categorie' => 'Food', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '32',
        'naam' => 'Starbucks',
        'imageUrl' => '/filters/food.svg',
        'categoie' => 'Food', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '33',
        'naam' => 'Swirl`s',
        'imageUrl' => '/filters/food.svg',
        'categorie' => 'Food', 'backgroundColor' => '#424242',
      ),
      array(
        'id' => '34',
        'naam' => 'The Döner Company',
        'imageUrl' => '/filters/food.svg',
        'categorie' => 'Food', 'backgroundColor' => '#424242',
      ),
    );

    {
        DB::table("filters")->insert($filtersArray);
    }
  }
}
