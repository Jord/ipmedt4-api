<?php

use Illuminate\Database\Seeder;

class StationFiltersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run(){
     $stationFiltersArray = array(
       array(
        'stations_id' => '1',
        'filters_id' => '1',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166069,
        'longitude' => 4.482457
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '2',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166444,
        'longitude' => 4.483158
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '2',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166662,
        'longitude' => 4.481677
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '3',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166686,
        'longitude' => 4.481764
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '4',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166026,
        'longitude' => 4.482544
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '5',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166035,
        'longitude' => 4.482899
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '5',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166576,
        'longitude' => 4.481718
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '6',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166012,
        'longitude' => 4.482427
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '7',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166036,
        'longitude' => 4.482810
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '8',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166474,
        'longitude' => 4.481903
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '9',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166293,
        'longitude' => 4.482262
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '9',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166138,
        'longitude' => 4.482595
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '10',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.165478,
        'longitude' => 4.481817
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '11',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166294,
        'longitude' => 4.482798
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '12',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166063,
        'longitude' => 4.482617
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '12',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166484,
        'longitude' => 4.481770
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '12',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166550,
        'longitude' => 4.481852
      ),
      array(
        'stations_id' => '1',
        'filters_id' => '12',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166146,
        'longitude' => 4.482717
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '13',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.165576,
        'longitude' => 4.481581
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '13',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.165548,
        'longitude' => 4.482551
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '14',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166416,
        'longitude' => 4.482312
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '14',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166303,
        'longitude' => 4.482854
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '15',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.165786,
        'longitude' => 4.481594
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '15',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.165809,
        'longitude' => 4.481155
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '16',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166444,
        'longitude' => 4.482379
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '17',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166857,
        'longitude' => 4.482360
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '17',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166652,
        'longitude' => 4.482704
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '17',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166062,
        'longitude' => 4.481915
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '17',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166239,
        'longitude' => 4.481533
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '17',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.165697,
        'longitude' => 4.481925
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '18',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166271,
        'longitude' => 4.482657
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '19',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166636,
        'longitude' => 4.481864
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '20',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166286,
        'longitude' => 4.482967
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '21',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.165971,
        'longitude' => 4.482524
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '22',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166308,
        'longitude' => 4.482513,
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '23',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166571,
        'longitude' => 4.481934
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '24',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166154,
        'longitude' => 4.482282,
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '25',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166204,
        'longitude' => 4.483013
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '25',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166489,
        'longitude' => 4.481625
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '26',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166256,
        'longitude' => 4.482066
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '27',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.165281,
        'longitude' => 4.482940,
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '28',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166357,
        'longitude' => 4.481881,
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '29',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.165999,
        'longitude' => 4.481311
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '29',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitiude' => 52.165849,
        'longitude' => 4.481683
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '30',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166300,
        'longitude' => 4.482369
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '31',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166443,
        'longitude' => 4.482229
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '32',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.165893,
        'longitude' => 4.482610
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '33',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.166318,
        'longitude' => 4.481994
      ),
       array(
        'stations_id' => '1',
        'filters_id' => '34',
        'beschrijving' => 'Prow scuttle parrel provost Sail ho shrouds spirits boom mizzenmast yardarm.',
        'tijden' => '09:00 - 20:00',
        'latitude' => 52.165797,
        'longitude' => 4.482166
      ),



     );



     {
         DB::table("station_filters")->insert($stationFiltersArray);
     }
   }
}
